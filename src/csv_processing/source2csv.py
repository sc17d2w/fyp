import csv
import re
import nltk
from nltk.corpus import stopwords
english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))
def create_csv():
    path = "word_def.csv"
    with open(path, 'w', newline='') as fc:
        csv_head = ["word", "definition", "example"]
        f_csv = csv.DictWriter(fc, csv_head)
        f_csv.writeheader()


def write_csv(c1, c2, c3):
    path = "word_def.csv"
    with open(path, 'a+', newline='') as fw:
        csv_write = csv.writer(fw)
        data_row = [c1, c2, c3]
        csv_write.writerow(data_row)


def is_close(line):
    bracket_cnt = 0
    for letter in line:
        if letter == '(':
            bracket_cnt += 1
        elif letter == ')':
            bracket_cnt -= 1
    if bracket_cnt <= 0:
        return True
    else:
        return False


def is_split(line):
    if line.find(": *46") > -1:
        return True
    else:
        return False
def clean(s):
    s= str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    # for w in s.:
    #     if w not in english_words or len(w)<2:
    #         s = s.replace(w, ' ')
    s = [word for word in s.split(' ') if 3 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s

create_csv()
flag_in = False
with open(r'ldoce') as f:
    count = 0
    word_cnt = 0
    word = ''
    line = f.readline()
    print("word count: %d " % word_cnt)
    while line:
        count += 1
        if flag_in:
            if line.find('(8') > -1 or line.find('(9') > 1: # find and extract the definition of word
                while not is_close(line):
                    count += 1
                    line = line + f.readline()
                if is_split(line):
                    if len(line.split(": *46")) == 2:
                    #     [definition, example] = [line.split(": *46")[0], line.split(": *46")[1]]
                    # else:
                        [definition, example] = line.split(": *46")
                else:
                    definition = line
                    example = ""
                # line = line.replace('\n', '')
                # line = line.replace('*CB','').replace('*46','').replace('*44','').replace('*8A','').replace('*45','')
                # definition = re.findall('\(8(.+)\)\)', line, re.S)
                with open('definition', "a") as fd:
                    # print(definition)
                    fd.writelines(definition)
                    fd.writelines('\n')
                    definition = clean(definition)
                    example = clean(example)
                    write_csv(word, definition, example)
                flag_in = False
        elif line.find('((') > -1: # find the word entry and extract the word itself
            print('count:%d'%count)
            word_cnt += 1
            word = re.findall('\(\((.*?)\)', line)
            with open('testTxt', "a") as ftw:
                ftw.writelines(word)
                ftw.writelines('\n')
            flag_in = True
        line = f.readline()
        # if count > 100000:
        #     break
        if word_cnt % 500 == 0:
            print("500 times %d words" % (word_cnt/500))
    print("word count: %d " % word_cnt)
# print(string)
# print(red)
# print( stop_words)