import csv
import re
import nltk
from nltk.corpus import stopwords
english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))
from nltk.stem import WordNetLemmatizer
wnl = WordNetLemmatizer()

def create_csv():
    path = "corpus_clean.csv"
    with open(path, 'w', newline='') as fc:
        csv_head = ["word", "definition", "example"]
        f_csv = csv.DictWriter(fc, csv_head)
        f_csv.writeheader()

def write_file(line):
    path = "ldoce_v3"
    with open(path, 'a+', newline='') as fw:
        fw.writelines(line)
        fw.writelines('\n')

def write_csv(c1, c2, c3):
    path = "corpus_clean.csv"
    with open(path, 'a+', newline='') as fw:
        csv_write = csv.writer(fw)
        data_row = [c1, c2, c3]
        csv_write.writerow(data_row)

def is_in_remove_list(line):
    remove_list = ('(1','(2','(3','(4','(5','(6','(7','9','(10')
    for key in remove_list:
        if line.find(key) > -1:
            return True
    return False

def is_close(line):
    bracket_cnt = 0
    for letter in line:
        if letter == '(':
            bracket_cnt += 1
        elif letter == ')':
            bracket_cnt -= 1
    if bracket_cnt <= 0:
        return True
    else:
        return False


def is_split(line):
    if line.find(": *46") > -1:
        return True
    else:
        return False

def clean(s):
    s= str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    for word in s.split(' '):
        if word.find('-') > -1:
            s = s.replace(word, ' '.join(word.split('-')))
    s = [wnl.lemmatize(word.lower()) for word in s.split(' ')]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if 3 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s

create_csv()
flag_in = False
with open(r'ldoce_processed') as f:
    count = 0
    word_cnt = 0
    word = ''
    line = f.readline()
    print("word count: %d " % word_cnt)
    while line:
        count += 1
        if flag_in:
            if is_in_remove_list(line):
                line = f.readline()
                continue
            if line.find('(8') > -1 or line.find('(9') > 1: # find and extract the definition of word
                while not is_close(line):
                    count += 1
                    line = line + f.readline()
                if is_split(line):
                    if len(line.split(": *46")) == 2:
                        [definition, example] = line.split(": *46")
                    else:
                        definition = line.split(": *46")[0]
                        example = line.split(": *46")[1]
                else:
                    definition = line
                    example = ""

                definition = clean(definition)
                example = clean(example)
                write_csv(word, definition, example)
            if line.find('((') > -1:
                flag_in = False
                continue
        elif line.find('((') > -1: # find the word entry and extract the word itself
            pare_cnt = 0
            # print('count:%d'%count)
            word_cnt += 1
            if not is_close(line[1:]):
                line = line + f.readline()
            word = str(re.findall('\(\((.*?)\)', line, re.S)[0])
            if word.lower() in stop_words:
                line = f.readline()
                continue
            with open('testTxt', "a") as ftw:
                ftw.writelines(word)
                ftw.writelines('\n')
            flag_in = True
        line = f.readline()
        # if count > 200:
        #     break
        if word_cnt % 500 == 0:
            print("500 times %d words" % (word_cnt/500))
    print("word count: %d " % word_cnt)
# print(string)
# print(red)
# print( stop_words)