import pandas as pd
import re
from gensim.models import KeyedVectors, Word2Vec
from nltk.corpus import stopwords
import nltk

stop_words = set(stopwords.words('english'))
words = set(nltk.corpus.words.words())
def clean(s):
    s= str(s)
    # s = s.replace('\r', '')
    # s = s.replace('\n', '')
    # s = re.sub('[!@#();:,*$1234567890]', '', s)
    for w in s.split(' '):
        if w not in words or len(w)<2:
            s = s.replace(w, ' ')
    # s = [word for word in s.split(' ') if word not in stop_words]
    # s = ' '.join(s)
    return s


df = pd.read_csv('word_def.csv')
# corpus_text = '\n'.join(df['definition'])
# sentences = corpus_text.split('\n')
# sentences = [line.lower().split(' ') for line in sentences]
# sentences = [clean(s) for s in sentences if len(s) > 0]
# print(sentences)
# for i in range(len(df['definition'])):
for i in range(1):
    df.definition[i] = clean(df.definition[i])
    df.example[i] = clean(df.example[i])
df.to_csv('out.csv', index=False, sep=',')

# sentences = [clean(s) for s in sentences if len(s) > 0]
# model = Word2Vec(sentences, size=300, window=10, min_count=1, workers=4, iter=5, sg=1)
# vectors = model.wv
# vectors.save('ldoce_3000_vec')
