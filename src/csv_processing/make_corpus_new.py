import csv
import re
import nltk
from nltk.corpus import stopwords
from nltk import WordNetLemmatizer

wnl = WordNetLemmatizer()
english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))

def write_file(line):
    path = "ldoce_v3"
    with open(path, 'a+', newline='') as fw:
        fw.writelines(line)
        fd.writelines('\n')

def is_in_remove_list(line):
    remove_list = ('(1','(2','(3','(4','(5','(6','(7','(10')
    for key in remove_list:
        if line.find(key) > -1:
            return True
    return False

def is_close(line):
    bracket_cnt = 0
    for letter in line:
        if letter == '(':
            bracket_cnt += 1
        elif letter == ')':
            bracket_cnt -= 1
    if bracket_cnt <= 0:
        return True
    else:
        return False


def is_split(line):
    if line.find(": *46") > -1:
        return True
    else:
        return False


def clean(s):
    s= str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    # for w in s.:
    #     if w not in english_words or len(w)<2:
    #         s = s.replace(w, ' ')
    # s = [word for word in s.split(' ') if 3 <= len(wnl.lemmatize(word)) <= 12]
    # s = ' '.join(s)
    for word in s.split(' '):
        if word.find('-') > -1:
            s = s.replace(word, ' '.join(word.split('-')))
    s = [wnl.lemmatize(word.lower()) for word in s.split(' ')]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if 3 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s

flag_in = False
with open(r'ldoce_processed') as f:
    count = 0
    word_cnt = 0
    word = ''
    line = f.readline()
    print("word count: %d " % word_cnt)
    while line:
        count += 1
        if flag_in:
            if is_in_remove_list(line):
                line = f.readline()
                continue
            if line.find('(8') > -1 or line.find('(9') > 1: # find and extract the definition of word
                while not is_close(line):
                    count += 1
                    line = line + f.readline()
                if is_split(line):
                    if len(line.split(": *46")) == 2:
                        [definition, example] = line.split(": *46")
                    else:
                        definition = line.split(": *46")[0]
                        example = line.split(": *46")[1]
                else:
                    definition = line
                    example = ""
                # line = line.replace('\n', '')
                # line = line.replace('*CB','').replace('*46','').replace('*44','').replace('*8A','').replace('*45','')
                # definition = re.findall('\(8(.+)\)\)', line, re.S)
                with open('corpus_full_lem_txt', "a") as fd:
                    # print(definition)
                    fd.writelines('\n')
                    definition = clean(definition)
                    example = clean(example)
                    fd.writelines(definition)
                    fd.writelines('\n')
                    fd.writelines(example)
            if line.find('((') > -1:
                flag_in = False
                continue
        elif line.find('((') > -1: # find the word entry and extract the word itself
            pare_cnt = 0
            print('count:%d'%count)
            word_cnt += 1
            if not is_close(line[1:]):
                line = line + f.readline()
            word = str(re.findall('\(\((.*?)\)', line, re.S)[0])
            with open('testTxt', "a") as ftw:
                ftw.writelines(word)
                ftw.writelines('\n')
            flag_in = True
        line = f.readline()
        # if count > 2000:
        #     break
        if word_cnt % 500 == 0:
            print("500 times %d words" % (word_cnt/500))
    print("word count: %d " % word_cnt)
# print(string)
# print(red)
# print( stop_words)