# import csv
# import re
#
#
# def create_csv():
#     path = "testu.csv"
#     with open(path, 'w') as fc:
#         csv_head = ["word", "definition"]
#         f_csv = csv.DictWriter(fc, csv_head)
#         f_csv.writeheader()
#
#
# def write_csv(c1, c2):
#     path = "testu.csv"
#     with open(path, 'a+') as fw:
#         csv_write = csv.writer(fw)
#         data_row = [c1, c2]
#         csv_write.writerow(data_row)
#
#
# create_csv()
# flag_in = False
# with open(r'testTxt') as f:
#     count = 0
#     word_cnt = 0
#     word = ''
#     line = f.readline()
#     print("word count: %d " % word_cnt)
#     while line:
#         count += 1
#         if flag_in:
#             if line.find('(8') > -1:
#                 while not line.endswith('))\n'):
#                     line = line + f.readline()
#                 # line = line.replace('\n', '').replace('      ', ' ').replace(' *CA ','').replace('*64','')
#                 # line = line.replace('*CB','').replace('*46','').replace('*44','').replace('*8A','').replace('*45','')
#                 definition = re.findall('\(8(.+)\)\)', line, re.S)
#                 print("line:{}".format(line))
#                 print("definition:{}".format(definition))
#                 with open('definition', "a") as fd:
#                     # print(definition)
#                     fd.writelines(definition)
#                     fd.writelines('\n')
#                     write_csv(word, definition)
#                 flag_in = False
#         elif line.find('((') > -1:
#             word_cnt += 1
#             word = re.findall('\(\((.*?)\)', line)
#             print(word)
#             flag_in = True
#         line = f.readline()
#         # if count > 15:
#         #     break
#     print("word count: %d " % word_cnt)
# # print(string)
# # print(red)
import nltk
import re
import gensim
import string
from collections import Counter
from string import punctuation
from nltk.tokenize import word_tokenize
from gensim.models import Word2Vec
from nltk.corpus import stopwords
#
# from nltk.tokenize import sent_tokenize, word_tokenize
#
# data = "All work and no play makes jack dull boy. All work and no play makes jack a dull boy."
# words = word_tokenize(data)
# print(words)
import pandas as pd
import csv
# def clean(s):
#     print(s)
#     s = s.replace('\r', '')
#     s = s.replace('\n', '')
#     print(s)
#     return s
# df = pd.read_csv('word_def.csv')
# words = df['word']
# for i in range(6):
#     df.definition[i] = clean(df.definition[i])
# df.to_csv('out.csv', index=False, sep=',')

# s = "a\nwad\nqwepoqe"
# s2 = "wew" \
#      "s" \
#      "dsd"
# print(s)
# s = s.replace('\n','')
# print(s)
# print('_____')
# print(s2)
# s2.replace('\n','')
# print(s2)
# import nltk
# from nltk.corpus import stopwords
words = set(nltk.corpus.words.words())
# stop_words  = set(stopwords.words('english'))
#
# sent = "Io andiamo to the beach of where when with my amico."
# sent = [word for word in sent.split(' ') if word not in stop_words]
# sent = ' '.join(sent)
# sent = " ".join(w for w in nltk.wordpunct_tokenize(sent) \
#          if w.lower() in words or not w.isalpha())
# print(sent)
# # 'Io to the beach with my'
# for i in range(0):
#     print(i)

# # print('distance between %s and %s: %.3f' % ('wqeqwe', 'zxczxczx', 1.5))
#
# df = pd.read_csv('word_def.csv')
# dd = df[df['word'].isin(['abandon'])]['definition'].tolist()
# print(dd[0])
import numpy as np
#
# sims = np.linspace(5, 14, 10)
# sims[sims.argmax()] = 1
# print(sims)
# print(len('eadasd wadawd '.split(' ')))
# from gensim.test.utils import common_texts
# from gensim.corpora import Dictionary
# print(common_texts)
# print('___________\n_____________\n__________')
# dct = Dictionary(common_texts)
#
# print(dct.doc2bow(['survey','survey','computer','interface','wewe']))

def write_file(line):
    path = "ldoce_v3"
    with open(path, 'w', newline='') as fw:
        if line.__class__ is list:
            for l in line:
                fw.writelines(l)
                fw.writelines('\n')
        else:
            fw.writelines(line)
            fw.writelines('\n')

# test_line = 'woidjwoij\nwaodijawio\n\rwa\roijdwa'
# write_file(test_line)
# def is_in_remove_list(line):
#     remove_list = ('(1','(2','(3','(4','(5','(6','(7','(10')
#     for key in remove_list:
#         if line.find(key) > -1:
#             return True
#     return False
#
# line = "((!< USAGE When *45 action *44 is used as a count\
#       noun it means the same thing as *45 act *44 : *46 a\
#       kind *45 act / action *44 !. Certain fixed phrs !.\
#       use *45 act *44 and not *45 action *44 : *46 an *45\
#       act *46 of cruelty / of mercy !| caught in the *45\
#       act *46 of stealing !. !| *45 Action *44 !, unlike\
#       *45 act *44 !, is also used as a mass noun *45 a\
#       *44 to mean the way or effect of doing something :\
#       *46 the *45 action *46 of a runner / of a medicine\
#       *45 b *44 in certain other fixed phrs !. : *46 to\
#       take (quick) *45 action *44 (= to act (quickly))\
#       !."
# # line_list = line.split(": *46")
# # new_list = []
# # for l in line_list:
# #     new_list.append(l.split('!.'))
# # write_file(new_list)
# # print(print(new_list))
# # print(line[1:])
# word = str(re.findall('\(\((.*?)\)', line, re.S)[0])
# print(word)

# sims = np.linspace(5, 14, 10)
# print(sims)
# print(sims.argmax())
# for idx, it in enumerate(sims):
#     if [sims.argmax()] < sims.index(sims) > it:
#         sims[sims.argmax()] = idx
#     print(sims)
# s = ' the hard or solid central part containing the seeds'
# print(s.split())
# print(s.split(' '))
# s1 = 'cloud'
# print(s1.split())
# print(s1.split(' '))
# cosine_sim = np.linspace(0, 0.1, 10)
# cosine_index = np.linspace(0, 0.1, 10)
# print(cosine_index)

from nltk.stem import WordNetLemmatizer

wnl = WordNetLemmatizer()
# s= 'students student aodijwia23 234 * 324 keeps high-level made makes make studies'
# #s = [wnl.lemmatize(word) for word in s.split(' ') if 2 <= len(word) <= 12]
#
# for word in s.split(' '):
#     if word.find('-')>-1:
#         s = s.replace(word, ' '.join(word.split('-')))
#
#
s = 'look: behaviour pyjama quoit tyre colour asdw'
s = [wnl.lemmatize(word) for word in s.split(' ') if word in words]
print(s)
#
# print(wnl.lemmatize('tired'))
# if 'ca'.lower() in words:
#     print('Y')
# else:
#     print('N')
# print()

# with open(r'corpus_full_lem_txt') as f:
#     line = f.readline()
#     cnt = 0
#     while line:
#         cnt += len(line.split())
#         line = f.readline()
#
#     print(cnt)
