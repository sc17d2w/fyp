# Import and download stopwords from NLTK.
from nltk.corpus import stopwords
import pandas as pd

defs = pd.read_csv('word_def.csv')
corpus_text = '\n'.join(defs['definition'])
sentences = corpus_text.split('\n')
sentences = [line.lower().split(' ') for line in sentences]


# Remove stopwords.
removed_list = []
stop_words = stopwords.words('english')
for sentence in sentences:
    removed_list.append([w for w in sentence if w not in stop_words])
# Prepare a dictionary and a corpus.
from gensim import corpora
documents = removed_list
dictionary = corpora.Dictionary(documents)

# Convert the sentences into bag-of-words vectors.
doc_list = []
for doc in documents:
    doc_list.append(dictionary.doc2bow(doc))

from itertools import chain
import json
from re import sub
from os.path import isfile

import gensim.downloader as api
from gensim.utils import simple_preprocess
from nltk.corpus import stopwords
from nltk import download
def preprocess(doc):
    doc = sub(r'<img[^<>]+(>|$)', " image_token ", doc)
    doc = sub(r'<[^<>]+(>|$)', " ", doc)
    doc = sub(r'\[img_assist[^]]*?\]', " ", doc)
    doc = sub(r'http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+', " url_token ", doc)
    return [token for token in simple_preprocess(doc, min_len=0, max_len=float("inf")) if token not in stopwords]

print(preprocess("mother fucker"))