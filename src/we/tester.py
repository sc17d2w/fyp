from gensim.models import KeyedVectors, Word2Vec, doc2vec

# Load pretrained model (since intermediate data is not included, the model cannot be refined with additional data)
# en_model = KeyedVectors.load_word2vec_format('wiki.en.vec')
# words = []
# en_model.save('wiki_en_model')
# en_model.init_sims(replace=True)
# en_model.save('wiki_en_model')
# for word in en_model.vocab:
#     words.append(word)
#
# # Printing out number of tokens available
# print("Number of Tokens: {}".format(len(words)))
#
# # Printing out the dimension of a word vector
# # print("Dimension of a word vector: {}".format(
# #     len(en_model[words[0]])
# # ))
#
# # Print out the vector of a word
# print("Vector components of a word: {}".format(
#     en_model[words[0]]
# ))
#
# # Pick a word
# find_similar_to = 'apple'
#
# for similar_word in en_model.similar_by_word(find_similar_to):
#     print("Word: {0}, Similarity: {1:.2f}".format(
#         similar_word[0], similar_word[1]
#     ))
line = "wraioj(())((): *46\n wiaojdiwaod"
# bracket_cnt = 0
# for letter in line:
#     if letter == '(':
#         bracket_cnt += 1
#     elif letter == ')':
#         bracket_cnt -= 1
#     if bracket_cnt == 0:
#         break
[definition, example] = line.split(": *46")
print([definition, example])