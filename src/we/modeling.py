def clean(s):
    s = str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    # for w in s.:
    #     if w not in english_words or len(w)<2:
    #         s = s.replace(w, ' ')
    # s = [word for word in s.split(' ') if 3 <= len(word) <= 12]
    # s = ' '.join(s)
    s = [word for word in s.split(' ') if 2 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s

def top_10_similar_words(keyword):
    keyword = clean(keyword)
    if keyword == "":
        return
    v = 0
    word_arr = [str(n) for n in keyword.split()]
    for single_word in word_arr:
            v += model.word_vec(single_word)
    v= v/(len(keyword.split())+0.0000001)
    print(v)
    print('___________________________________')
    try:
        for similar_word in model.similar_by_vector(v, topn=30):
            # if similar_word not in df.word:
            #     continue
            print("Word: {0}, Similarity: {1:.2f}".format(
                similar_word[0], similar_word[1]
            ))
    except KeyError:
        print('The word no in vocabulary')
    s1 = 'the first sentence in first document'
    s2 = 'the first sentence of the first text'
    distance = model.wmdistance(s1,s2)
    distance = model.wmdistance(s1, 'the first pig fuck in first document')
    print('distance = %.3f' % distance)

import nltk
from gensim.models import KeyedVectors, doc2vec
import pyemd
import re
from nltk.corpus import stopwords
import pandas as pd

english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))
df = pd.read_csv('corpus_full.csv')

model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)
# model = KeyedVectors.load('ldoce_3000_vec', mmap='r')
# model = KeyedVectors.load('googlenews_vec', mmap='r')
# model.wv.save('googlenews_vec')
# result = model.evaluate_word_pairs('SimLex-999.txt')
# # # model = KeyedVectors.load_word2vec_format('googlenews.txt', binary=False)
# # analogy_scores = model.wv.evaluate_word_analogies(datapath('questions-words.txt'))
# print('___________simlex____________')
# print(result)
# for i in result:
#     print(i)
keyword = ''
# print('___________end1____________')
# result2 = model.evaluate_word_pairs('questions-words.txt')
# print(result2)
# for i in result2:
#     print(i)
print('___________end2____________')

result3 = model.evaluate_word_pairs('wordsim353.tsv')
print(result3)
for i in result3:
    print(i)
print('___________end1____________')
while keyword != 'exit()':
    keyword = input('Enter a word:')
    top_10_similar_words(keyword)
