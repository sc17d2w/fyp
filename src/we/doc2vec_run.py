import collections
import random

import gensim
import numpy as np

# model = gensim.models.Doc2Vec.load('saved_doc2vec_model')

# new_sentence = "I opened a new mailbox".split(" ")
# for ss in model.docvecs.most_similar(positive=[model.infer_vector(new_sentence)],topn=5):
#     print(ss)

# def avg_sentence_vector(words, model, num_features, index2word_set):
#     #function to average all words vectors in a given paragraph
#     featureVec = np.zeros((num_features,), dtype="float32")
#     nwords = 0
#
#     for word in words:
#         if word in index2word_set:
#             nwords = nwords+1
#             featureVec = np.add(featureVec, model[word])
#
#     if nwords>0:
#         featureVec = np.divide(featureVec, nwords)
#     return featureVec
#
#
# #get average vector for sentence 1
# sentence_1 = "this is sentence number one"
# sentence_1_avg_vector = avg_feature_vector(sentence_1.split(), model=word2vec_model, num_features=100)
#
# #get average vector for sentence 2
# sentence_2 = "this is sentence number two"
# sentence_2_avg_vector = avg_feature_vector(sentence_2.split(), model=word2vec_model, num_features=100)
#
# sen1_sen2_similarity =  cosine_similarity(sentence_1_avg_vector,sentence_2_avg_vector)
import smart_open

from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
from gensim.models import Word2Vec

model2 = Word2Vec()


corpus_text = []
with open(r'corpus_full_txt') as f:
    line = f.readline()
    while line:
        corpus_text.append(line.replace('\n',''))
        line = f.readline()
# print(corpus_text)
corpus_text = filter(None, corpus_text)
stoplist = set('for a of the and to in'.split())
texts = [[word for word in str(document).lower().split() ]
         for document in corpus_text]
print(common_texts)
print(texts[:20])
# texts = texts[:20]
documents = [TaggedDocument(doc, [i]) for i, doc in enumerate(texts)]


model = Doc2Vec.load('my_doc2vec_model')
new_sentence = "visible mass condensed watery vapour floating atmosphere".split(" ")
# new_sentence = "holding wires which moved".split(" ")
print(new_sentence)
for ss in model.docvecs.most_similar([model.infer_vector(new_sentence)],topn=10):
    print('similarity %.3f' % ss[1])
    print(' '.join(documents[ss[0]].words))


ranks = []
second_ranks = []
# for doc_id in range(len(documents)):
#     inferred_vector = model.infer_vector(documents[doc_id].words)
#     sims = model.docvecs.most_similar([inferred_vector], topn=len(model.docvecs))
#     rank = [docid for docid, sim in sims].index(doc_id)
#     ranks.append(rank)
#     second_ranks.append(sims[1])

# doc_id = random.randint(0, len(documents) - 1)
#
# # Compare and print the second-most-similar document
# print('Train Document ({}): «{}»\n'.format(doc_id, ' '.join(documents[doc_id].words)))
# sim_id = second_ranks[doc_id]
# print('Similar Document {}: «{}»\n'.format(sim_id, ' '.join(documents[sim_id[0]].words)))


# model.save('my_doc2vec_model')
# model = Doc2Vec.load(fname)  # you can continue training with the loaded model!