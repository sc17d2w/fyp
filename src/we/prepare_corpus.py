import pandas as pd
from gensim.models import KeyedVectors, Word2Vec
# def clean(s):
#     return [w.strip(',."!?:;*()\'') for w in s]
corpus_text = []
with open(r'corpus_full_lem_txt') as f:
    line = f.readline()
    while line:
        corpus_text.append(line.replace('\n',''))
        line = f.readline()
# print(corpus_text)
corpus_text = filter(None, corpus_text)
stoplist = set('for a of the and to in'.split())
texts = [[word for word in str(document).lower().split() ]
         for document in corpus_text]
# sentences = corpus_text.split('\n')
# sentences = [line.lower().split(' ') for line in sentences]

#
# sentences = [clean(s) for s in sentences if len(s) > 0]
model = Word2Vec(texts, size=50, window=10, min_count=1, workers=4, iter=10, sg=1, hs=1, alpha=0.015)
print("iter=%d, size=%d, alpha=%.3f, sg=%d, hs=%d" % (model.iter, model.vector_size, model.alpha, model.sg, model.hs))
print("\nwsm353:"+str(model.evaluate_word_pairs('wordsim353.tsv')))
print("\nsim999:" + str(model.evaluate_word_pairs('simlex-english.txt')))
with open('training_record', 'a') as f:
    f.write("iter=%d, size=%d, alpha=%.3f, sg=%d, hs=%d" % (model.iter, model.vector_size, model.alpha, model.sg, model.hs))
    f.write("\nwsm353:"+str(model.evaluate_word_pairs('wordsim353.tsv')))
    f.write("\nsim999:" + str(model.evaluate_word_pairs('simlex-english.txt')))
    f.write('\n-------------------------------------\n')

# print(model.evaluate_word_pairs('SimLex-999.txt'))
model.wv.save_word2vec_format('ldoce_clean_model_150d_20it.bin')
vectors = model.wv
vectors.save('ldoce_clean_vec_150d_20it')