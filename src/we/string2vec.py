import logging
from gensim.models import Word2Vec
documents = ["Human machine interface for lab abc computer applications",
             "A survey of user opinion of computer system response time",
             "The EPS user interface management system",
             "System and human system engineering testing of EPS"]

from pprint import pprint
from collections import defaultdict

# remove common words and tokenize
stoplist = set('for a of the and to in'.split())
texts = [[word for word in document.lower().split() if word not in stoplist]
         for document in documents]

# remove words that appear only once
frequency = defaultdict(int)
for text in texts:
    for token in text:
        frequency[token] += 1

texts = [[token for token in text if frequency[token] > 1]
         for text in texts]
model = Word2Vec(min_count=1)
pprint(texts)
model.build_vocab(texts)
model.train(texts, total_examples=model.corpus_count, epochs=model.iter)
keyword = "of"
v = 0
word_arr = [str(n) for n in keyword.split()]
for single_word in word_arr:
    v += model.wv.word_vec(single_word)
try:
    for similar_word in model.similar_by_vector(v):
        print("Word: {0}, Similarity: {1:.2f}".format(
            similar_word[0], similar_word[1]
        ))
except KeyError:
    print('The word no in vocabulary')