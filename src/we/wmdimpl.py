from gensim.models import KeyedVectors, doc2vec, Word2Vec
from gensim.test.utils import common_texts
import pyemd
from gensim.similarities import MatrixSimilarity, WmdSimilarity, SoftCosineSimilarity


def wmdist(s1, s2):
    distance = en_model.wmdistance(s1,s2)
    print('distance = %.3f' % distance)
    # model = Word2Vec(common_texts, size=20, min_count=1)
    # index = WmdSimilarity(common_texts, model)
    # query = [s1]
    # sims = index[query]
    # print('sims: %.3f' % sims)
    # en_model.w


if __name__ == '__main__':
    # Load pretrained model (since intermediate data is not included, the model cannot be refined with additional data)
    # en_model = KeyedVectors.load('ldoce_3000_vec', mmap='r')
    en_model = KeyedVectors.load('wiki_en_model', mmap='r')
    words = []
    for word in en_model.vocab:
        words.append(word)

    # Printing out number of tokens available
    print("Number of Tokens: {}".format(len(words)))
    # Searching interface
    keyword = ''
    while keyword != 'exit()':
        s1 = input('Enter first sentence:')
        s2 = input('Enter second sentence:')
        wmdist(s1, s2)