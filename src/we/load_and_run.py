import re

import nltk
from gensim.models import KeyedVectors, doc2vec
import pyemd
from nltk.corpus import stopwords
import pandas as pd

english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))


def get_vector(word):
    vector = en_model[word]
    return vector


def top_10_similar_words(keyword):
    keyword = clean(keyword)
    if keyword == "":
        return
    v = 0
    word_arr = [str(n) for n in keyword.split()]
    for single_word in word_arr:
        v += en_model.word_vec(single_word)
    try:
        for similar_word in en_model.similar_by_vector(v):
            print("Word: {0}, Similarity: {1:.2f}".format(
                similar_word[0], similar_word[1]
            ))
    except KeyError:
        print('The word no in vocabulary')
    s1 = "weightless mass of very small of water floating high in the air".split()
    s2 = "film that under are not admitted to see in cinema compare'".split()
    print(s1)
    distance = en_model.wmdistance(s1, s2)
    print('distance = %.3f' % distance)


def clean(s):
    s = str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    # for w in s.:
    #     if w not in english_words or len(w)<2:
    #         s = s.replace(w, ' ')
    # s = [word for word in s.split(' ') if 3 <= len(word) <= 12]
    # s = ' '.join(s)
    s = [word for word in s.split(' ') if 2 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s


if __name__ == '__main__':
    df = pd.read_csv('corpus_full.csv')
    # Load pretrained model (since intermediate data is not included, the model cannot be refined with additional data)
    # en_model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)
    en_model = KeyedVectors.load('ldoce_full_vec_20it', mmap='r')
    result2 = en_model.evaluate_word_pairs('wordsim353.tsv')
    print(result2)
    # en_model = KeyedVectors.load('wiki_en_model', mmap='r')
    # model = KeyedVectors.load_word2vec_format("")
    words = []
    for word in en_model.vocab:
        words.append(word)

    # Printing out number of tokens available
    print("Number of Tokens: {}".format(len(words)))
    # Searching interface
    keyword = ''
    vec = get_vector('queen')
    vec2 = get_vector('king')
    print(en_model.n_similarity(['queen'], ['book']))
    while keyword != 'exit()':
        keyword = input('Enter a word:')
        if keyword == "2":
            pass
        stoplist = set('for a of the and to in'.split())

        top_10_similar_words(keyword)
