from gensim.test.utils import datapath
from gensim.models import KeyedVectors
import pandas as pd
import numpy as np
import nltk
from nltk.corpus import stopwords
import re
from nltk import WordNetLemmatizer

english_words = set(nltk.corpus.words.words())
stop_words = set(stopwords.words('english'))
wnl = WordNetLemmatizer()

def clean(s):
    s= str(s)
    s = s.replace('\r', '')
    s = s.replace('\n', '')
    s = re.sub('[!@#();:,*$1234567890]', '', s)
    # for w in s.:
    #     if w not in english_words or len(w)<2:
    #         s = s.replace(w, ' ')
    # s = [word for word in s.split(' ') if 3 <= len(wnl.lemmatize(word)) <= 12]
    # s = ' '.join(s)
    for word in s.split(' '):
        if word.find('-') > -1:
            s = s.replace(word, ' '.join(word.split('-')))
    s = [wnl.lemmatize(word.lower()) for word in s.split(' ')]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if 3 <= len(word) <= 12 and word in english_words]
    s = ' '.join(s)
    s = [word for word in s.split(' ') if word.lower() not in stop_words]
    s = ' '.join(s)
    return s

def top_10_similar_words(keyword):
    v = 0
    word_arr = [str(n) for n in keyword.split()]
    for single_word in word_arr:
            v += model.word_vec(single_word)
    try:
        for similar_word in model.similar_by_vector(v):
            print("Word: {0}, Similarity: {1:.2f}".format(
                similar_word[0], similar_word[1]
            ))
    except KeyError:
        print('The word no in vocabulary')
    s1 = 'the first sentence in first document'
    s2 = 'the first sentence of the first text'
    distance = model.wmdistance(s1,s2)
    print('distance = %.3f' % distance)


def find_def_by_word(word):
    if len(word) > 1:
        return word
    definition = word
    # for idx, w in enumerate(df.word):
    #     if w == word:
    #         definition = df.definition[idx]
    #         break
    result = df[df['word'].isin([word])]['definition'].tolist()[0]
    if result != '':
        definition = result
    return definition


def most_similar_by_definitions(keyword):
    cnt_2 = 0
    distancelist = []
    topn = 10
    wmd_distance = np.linspace(5, 14, topn)
    wmd_index = np.linspace(5, 14, topn)
    cosine_sim = np.linspace(0, 0.1, topn)
    cosine_index = np.linspace(0, 0.1, topn)
    def_of_keyword = find_def_by_word(keyword)
    def_of_keyword= clean(def_of_keyword).split()
    print("%s : %s"%(keyword, def_of_keyword))
    for idx, definition in enumerate(df.definition):
        # if idx > 18000:
        #     break
        # print("word:%s def: %s" % (df.word[idx], definition))
        # cnt_2 += 1
        # if cnt_2 > 3000:
        #     break
        definition = clean(str(definition))
        if definition == '' or df.word[idx] == keyword:
            continue
        definition = definition.split()
        try:
            distance = model.wmdistance(def_of_keyword, definition)
            similarity = model.n_similarity(def_of_keyword, definition)
        except Exception as error:
            print('position measure : '+str(error))
            pass
        # distancelist.append(distance)
        wmd_max_index = wmd_distance.argmax()
        cos_min_index = cosine_sim.argmin()
        if distance < wmd_distance[wmd_max_index]:
            wmd_index[wmd_max_index] = idx
            wmd_distance[wmd_max_index] = distance
        if similarity > cosine_sim[cos_min_index]:
            cosine_index[cos_min_index] = idx
            cosine_sim[cos_min_index] = similarity
    # print(wmd_index)
    # print(wmd_distance)

    try:
        for cnt,i in enumerate(wmd_index):
            print('word: %s distance: %.3f definition: %s' % (df.word[i], wmd_distance[cnt], df.definition[i]))
        for cnt,i in enumerate(cosine_index):
            print('word: %s similarity: %.3f definition: %s' % (df.word[i], cosine_sim[cnt], df.definition[i]))
    except Exception as error:
        print(error)
        pass


df = pd.read_csv('corpus_full.csv')
model = KeyedVectors.load_word2vec_format('GoogleNews-vectors-negative300.bin.gz', binary=True)
# model = KeyedVectors.load('ldoce_clean_vec_200d_20it', mmap='r')
# model.wv.save('googlenews_vec')
# result = model.evaluate_word_pairs('SimLex-999.txt')
# # # model = KeyedVectors.load_word2vec_format('googlenews.txt', binary=False)
# # analogy_scores = model.wv.evaluate_word_analogies(datapath('questions-words.txt'))
# print('___________simlex____________')
# print(result)
# for i in result:
#     print(i)
keyword = ''
# print('___________end1____________')
# result2 = model.evaluate_word_pairs('questions-words.txt')
# print(result2)
# for i in result2:
#     print(i)
# print('___________end2____________')
#
# result3 = model.evaluate_word_pairs('wordsim353.tsv')
# print(result3)
# for i in result3:
#     print(i)
# print('___________end1____________')

while keyword != 'exit()':
    keyword = input('Enter a word:')
    most_similar_by_definitions(keyword)
    # keyword = clean(keyword).split()
    # s1 = clean('weightless mass of very small of water floating high in the air').split()
    # print(s1)
    # print(keyword)
    # print(model.wmdistance(s1, keyword))
    # print(model.n_similarity(s1, keyword))
    # if keyword not in model.vocab:
    #     print('NO')
    # else:
    #     print('Y')