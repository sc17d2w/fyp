import csv
import re


def create_csv():
    path = "word_def.csv"
    with open(path, 'w') as fc:
        csv_head = ["id","word", "definition"]
        f_csv = csv.DictWriter(fc, csv_head)
        f_csv.writeheader()


def write_csv(c0, c1, c2):
    path = "word_def.csv"
    with open(path, 'a+') as fw:
        csv_write = csv.writer(fw)
        data_row = [c0, c1, c2]
        csv_write.writerow(data_row)


create_csv()
flag_in = False
with open(r'ldoce') as f:
    count = 0
    word_cnt = 0
    word = ''
    line = f.readline()
    entry = 0
    while line:
        count += 1
        if flag_in:
            if line.find('(8') > -1: # find and extract the definition of word
                entry += 1
                while not line.endswith('))\n'):
                    line = line + f.readline()
                line = line.replace('\n', ' ').replace('      ', ' ').replace('*CA','').replace('*64','')
                line = line.replace('*CB','').replace('*46','').replace('*44','').replace('*8A','').replace('*45','')
                definition = re.findall('\(8(.+)\)\)', line, re.S)
                with open('definition', "a") as fd:
                    # print(definition)
                    fd.writelines(definition)
                    fd.writelines('\n')
                    write_csv(entry, word, definition)
                if line.find('((') > -1:
                    flag_in = False
        elif line.find('((') > -1: # find the word entry and extract the word itself
            word_cnt += 1
            word = re.findall('\(\((.*?)\)', line)
            with open('testTxt', "a") as ftw:
                ftw.writelines(word)
                ftw.writelines('\n')
            flag_in = True
        line = f.readline()
        if count > 5000:
            break
# print(string)
# print(red)
