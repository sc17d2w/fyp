from keras.models import Sequential
from keras.layers import Dense
import numpy

seed = 7
numpy.random.seed(seed)

#load dataset
dataset = numpy.loadtxt()
X = dataset[:, 0:8]
Y = dataset[:, 8]